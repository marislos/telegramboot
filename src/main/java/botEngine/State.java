package botEngine;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

public enum State {

    INITIALIZED(new String[]{"/start"}),

    START("/start", new String[]{"Go", "Help"}),

    GO("Go", new String[]{"Grammar", "Vocabulary", "Exercise", "Cards", "Back"}),

    HELP("Help", new String[]{"Go", "Help", "Hello"}),

    GRAMMAR("Grammar", new String[]{"Give me a grammar", "Back"}),

    VOCABULARY("Vocabulary", new String[]{"Word-picker", "Extra", "Back"}),

    EXERCISE("Exercise", new String[]{"Let's start", "Back"}),

    CARDS("Cards", new String[]{"Back", "Translation", "New word"}),

    WORD_PICKER("Word-picker", new String[]{"Extra", "Word-picker", "Back"}),

    EXTRA("Extra", new String[]{"Word-picker","Extra", "Back"}),

    GIVE_GRAMMAR("Give me a grammar", new String[]{"Give me a grammar", "Back"}),

    LETS_START("Let's start", new String[]{"Let's start", "Back"}),

    ANSWER("", new String[]{"Let's start", "Back"}),

    BACK("Back", new String[]{"Go", "Help", "Hello", "Grammar", "Vocabulary", "Exercise", "Cards",
            "Extra", "Word-picker", "Give me a grammar", "Let's start","New word", "Translation" }),

    TRANSLATION("Translation", new String[]{"Back","Cards", "New word", "Translation"}),

    NEW_WORD("New word", new String[]{"Back", "Translation", "New word"}),

    ERROR;

    private String command;
    private Set<String> availableCommands;

    State(String... availableCommands) {
        this.availableCommands = Sets.newHashSet(availableCommands);
    }

    State(String command, String... availableCommands) {
        this.command = command;
        this.availableCommands = Sets.newHashSet(availableCommands);
    }

    public String getCommand() {
        return command;
    }

    public Set<String> getAvailableCommands() {
        return availableCommands;
    }

    public static State getNextState(String command, State clientState) {
        for (State state : values())
            if (command.equals(state.getCommand()))
            {
                return state;
            }

        if (clientState == LETS_START && StringUtils.isNoneEmpty(command))
        {
            return ANSWER;
        }

        return ERROR;
    }
}
